// expose our config directly to our application using module.exports
var base_route = function(url){
  if (process.env["NODE_ENV"] != "production"){
    return "http://localhost:4000" + url;
  }else{
    return "http://internationalswagg.com" + url;
  }
}

module.exports = {
  'facebookAuth' : {
    'clientID'      : process.env['FB_APP_ID'],
    'clientSecret'  : process.env['FB_APP_SECRET'],
    'callbackURL'   : base_route('/login/facebook/callback'),
  },

  'googleAuth' : {
    'clientID'      : process.env['GOOGLE_CLIENT_ID'],
    'clientSecret'  : process.env['GOOGLE_CLIENT_SECRET'],
    'callbackURL'   : base_route('/login/google/callback')
  }
};
