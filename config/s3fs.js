var S3FS = require('s3fs');
var bucketPath = process.env['AWS_BUCKET'];
var s3Options = {
  accessKeyId: process.env['AWS_ACCESS_KEY_ID'],
  secretAccessKey: process.env['AWS_SECRET_ACCESS_KEY'],
  params: { ACL: "public-read-write", CacheControl: 'max-age=315576000'}
};
var s3fs = new S3FS(bucketPath, s3Options);

module.exports = s3fs;