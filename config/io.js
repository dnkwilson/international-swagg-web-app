// Socket
var redis = require('redis');
var db = redis.createClient(process.env['REDIS_URL']);
var Chat = require("../models/Chat");
var util = require("util");
var truncate = require('truncate');
var logger = require('morgan');

var request = require('request-json');
var api  = request.createClient(process.env["DC_API_HOST"]);

function setDefaultHeader(token){
  api.headers['Content-Type']  = 'application/json';
  api.headers['Accept']        = 'application/json';
  api.headers['Authorization'] = "Token token=\"" + token + "\"";
  api.headers['API-CALLER']    = "InternationalSwaggWebApp";
}

var push = function (message) {
  api.post('/v2/send_notification', message, function(err, response, body) {
    // Do something
  });
}

// usernames which are currently connected to the chat
var users = {};
var numUsers = 0;
var COLORS = [ '#e21400', '#91580f', '#f8a700', '#f78b00',
  '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
  '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
];

function getUsernameColor (username) {
  // Compute hash code
  var hash = 7;
  for (var i = 0; i < username.length; i++) {
     hash = username.charCodeAt(i) + (hash << 5) - hash;
  }
  // Calculate color
  var index = Math.abs(hash % COLORS.length);
  return COLORS[index];
}

module.exports = function(server){
  var io = require('socket.io')(server);
  // Chatroom
  io.on('connection', function (socket) {
    var addedUser = false;

    // when the client emits 'new message', this listens and executes
    socket.on('new message', function (data) {
      // return unless user is logged in
      if (data['user_id'] == 0) { return null };

      data['userColor'] = getUsernameColor(data['username']);

      var chat = new Chat(data);
      chat.save();

      // send push notification
      var str = util.format("%s: %s", chat.getUserName(), truncate(chat.getBody(), 60));
      var data = {
        "text": str,
        "title": "New message",
        "payload": {
          "uri":"com.damagecontrolhq.internationalswagg.fragments.ChatFragment"
        }
      };
      push(data);

      // we tell the client to execute 'new message'
      socket.broadcast.emit('new message', chat);
    });

    // when the client emits 'add user', this listens and executes
    socket.on('add user', function (user) {

      // we store the username in the socket session for this client
      socket.user = user;

      // add the client's username to the global list
      users[socket.user_id] = socket.user;
      ++numUsers;

      db.lrange("chat:messages", -200, -1, function (err, data) {
        var history = []
        for (var i = 0; i < data.length; i++) {
          history.push(JSON.parse(data[i]));
        };

        addedUser = true;
        socket.emit('login', { messages: history, user: socket.user});
      });

      // echo globally (all clients) that a person has connected
      socket.broadcast.emit('user joined', {
        user: socket.user,
        numUsers: numUsers
      });

      // push({"data": {"alert": socket.user.username + " entered the chatroom.", "title":"International Swagg", "uri":"com.dnwilson.internationalswagg.fragments.ChatFragment"}, "isBackground": false});
    });

    // when the client emits 'typing', we broadcast it to others
    socket.on('typing', function () {
      socket.broadcast.emit('typing', {
        username: socket.user
      });
    });

    // when the client emits 'stop typing', we broadcast it to others
    socket.on('stop typing', function () {
      socket.broadcast.emit('stop typing', {
        username: socket.user
      });
    });

    // when the user disconnects.. perform this
    socket.on('disconnect', function () {
      // remove the username from global usernames list
      if (addedUser) {
        delete users[socket.user.user_id];
        --numUsers;

        // echo globally that this client has left
        socket.broadcast.emit('user left', {
          username: socket.user,
          numUsers: numUsers
        });
      }
    });
  });
}
