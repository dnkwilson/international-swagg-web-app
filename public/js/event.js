jQuery(function($) {
  $imageArray  = []
  $galleryItem = $('<span class="item" />')
  
  var $galleryArea = $('.carousel-inner');
  $(".tab-content").find('.tab-pane').first().addClass("active");
  $(".tab-pane.active").find('.item').first().addClass('active');
  $('.nav-pills').find('li').first().addClass("active");

  $('.nav-pills').find('li a').click(function () {
    var id = this.getAttribute('href');
    $(id +'.tab-pane').find('.item').first().addClass('active');
  });

  $('.single-image a').colorbox({rel:'gallery', maxHeight:'90%'});

});
