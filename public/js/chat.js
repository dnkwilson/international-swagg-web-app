jQuery(function($) {
  var FADE_TIME = 150; // ms
  var TYPING_TIMER_LENGTH = 400; // ms
  var COLORS = [
    '#e21400', '#91580f', '#f8a700', '#f78b00',
    '#58dc00', '#287b00', '#a8f07a', '#4ae8c4',
    '#3b88eb', '#3824aa', '#a700ff', '#d300e7'
  ];

  // Initialize variables
  var $window = $(window);
  var $usernameInput = $('.usernameInput'); // Input for username
  var $messages = $('.messages'); // Messages area
  var $inputMessage = $('.inputMessage'); // Input message input box

  var $mainPage = $('#content'); // The login page
  var $chatPage = $('#chat'); // The chatroom page

  // Prompt for setting a username
  var username;
  var connected = false;
  var typing = false;
  var lastTypingTime;
  var $currentInput = $usernameInput.focus();

  var socket = io();

  function addParticipantsMessage (data) {
    var message = '';
    if (data.numUsers === 1) {
      message += "there's 1 participant";
    } else {
      message += "there are " + data.numUsers + " participants";
    }
    log(message);
  }

  $(".chat").click(setUser());
  $('.main-nav').addClass('fixed-menu animated slideInDown');

  // Sets the client's username
  function setUser () {
    // If the username is valid
    if (user) {
      $mainPage.fadeOut();
      $chatPage.show();
      $mainPage.off('click');
      $currentInput = $inputMessage.focus();

      // Tell the server your username
      socket.emit('add user', user);
    }
  }

  function Chat (message) {
    this.body = message;
    this.username = user.username;
    this.category = "text_message";
    this.user_id  = user.id;
    this.avatar   = user.avatar;
    this.time     = moment().format();
  }

  // Sends a chat message
  function sendMessage () {
    var message = $inputMessage.val();
    // Prevent markup from being injected into the message
    message = cleanInput(message);

    // if there is a non-empty message and a socket connection
    if (message && connected) {
      $inputMessage.val('');
      var chat = {
        username: user.username,
        body: message,
        time: moment().format(),
        category: "text_message",
        user_id: user.id,
        avatar: user.avatar
      }
      addChatMessage(chat);

      // tell server to execute 'new message' and send along one parameter
      socket.emit('new message', chat);
    }
  }

  // Log a message
  function log (message, options) {
    var $el = $('<li>').addClass('log').text(message);
    addMessageElement($el, options);
  }

  // Adds the visual chat message to the message list
  function addChatMessage (data, options) {
    // Set message type
    var messageClass = "message-in"
    var avatarAlign  = "right"
    if(data.user_id == user.id){
      // var messageOut = options.fromMe || data.user_id == user.user_id
      var messageOut = true;
      messageClass   = (messageOut ? "message-out" : "message-in");
      avatarAlign    = (messageOut ? "left" : "right");
    }

    if (data.avatar == null || data.avatar.charAt(0) == "/") {
      data.avatar = "/images/swagg_icon.png";
    };

    // Don't fade the message in if there is an 'X was typing'
    var $typingMessages = getTypingMessages(data);
    options = options || {};
    if ($typingMessages.length !== 0) {
      options.fade = false;
      $typingMessages.remove();
    }

    var $avatarDiv = $('<a href="'+data.avatar+'" class="col-xs-3 col-sm-2 avatar"/>')
      .append($('<img src="'+data.avatar+'" class="img-circle"/>'))
      .addClass(avatarAlign);

    $avatarDiv.colorbox();

    var $messageTextDiv = $('<span class="messageBody"/>');

    if (data.category != "voice_message") {
      var $usernameDiv = $('<div class="username"/>')
      .text(data.username)
      .css('color', getUsernameColor(data.username));
      $messageTextDiv.append($usernameDiv);
    }

    switch(data.category){
      case "photo_message":
        $imageDiv = $('<a href="/uploads/'+data.filename+'" class="photo_message" />')
        .append($('<img src="/uploads/'+data.filename+'" class="'+data.filename+'" />'));
        $messageTextDiv.append($imageDiv);
        $imageDiv.colorbox();
        break;
      case "voice_message":
        $audioDiv = $('<audio controls/>').append($('<source src="'+data.media+'" type="audio/mpeg"/>'));
        $messageTextDiv.append($audioDiv)
        break;
      default:
        $textDiv = $('<div class="messageBody">').text(data.body);
        $messageTextDiv.append($textDiv);
        break;
    }

    var $messageTimeDiv = $('<span class="messageTime right">')
      .text(moment(data.time).format("hh:mmA"));

    var typingClass = data.typing ? 'typing' : '';

    var $messageContainerDiv = $('<span class="col-xs-9 col-sm-10"/>')
      .append($('<span class="'+messageClass+'"/>').append($messageTextDiv, $messageTimeDiv));

    var $messageDiv = $('<li class="row message"/>')
      .data('username', data.username)
      .addClass(typingClass)

    if (messageOut) {
      $messageDiv.append($messageContainerDiv, $avatarDiv);
    } else{
      $messageDiv.append($avatarDiv, $messageContainerDiv);
    };


    addMessageElement($messageDiv, options);
  }

  // Adds the visual chat typing message
  function addChatTyping (data) {
    data.typing = true;
    data.message = 'is typing';
    addChatMessage(data);
  }

  // Removes the visual chat typing message
  function removeChatTyping (data) {
    getTypingMessages(data).fadeOut(function () {
      $(this).remove();
    });
  }

  // Adds a message element to the messages and scrolls to the bottom
  // el - The element to add as a message
  // options.fade - If the element should fade-in (default = true)
  // options.prepend - If the element should prepend
  //   all other messages (default = false)
  function addMessageElement (el, options) {
    var $el = $(el);

    // Setup default options
    if (!options) {
      options = {};
    }
    if (typeof options.fade === 'undefined') {
      options.fade = true;
    }
    if (typeof options.prepend === 'undefined') {
      options.prepend = false;
    }

    // Apply options
    if (options.fade) {
      $el.hide().fadeIn(FADE_TIME);
    }
    if (options.prepend) {
      $messages.prepend($el);
    } else {
      $messages.append($el);
    }
    window.scrollTo(0,document.body.scrollHeight);
    $messages[0].scrollTop = $messages[0].scrollHeight;
  }

  // Prevents input from having injected markup
  function cleanInput (input) {
    return $('<div/>').text(input).text();
  }

  // Updates the typing event
  function updateTyping () {
    if (connected) {
      if (!typing) {
        typing = true;
        socket.emit('typing');
      }
      lastTypingTime = (new Date()).getTime();

      setTimeout(function () {
        var typingTimer = (new Date()).getTime();
        var timeDiff = typingTimer - lastTypingTime;
        if (timeDiff >= TYPING_TIMER_LENGTH && typing) {
          socket.emit('stop typing');
          typing = false;
        }
      }, TYPING_TIMER_LENGTH);
    }
  }

  // Gets the 'X is typing' messages of a user
  function getTypingMessages (data) {
    return $('.typing.message').filter(function (i) {
      return $(this).data('username') === data.username;
    });
  }

  // Gets the color of a username through our hash function
  function getUsernameColor (username) {
    // Compute hash code
    var hash = 7;
    for (var i = 0; i < username.length; i++) {
       hash = username.charCodeAt(i) + (hash << 5) - hash;
    }
    // Calculate color
    var index = Math.abs(hash % COLORS.length);
    return COLORS[index];
  }

  // Keyboard events

  $window.keydown(function (event) {
    // Auto-focus the current input when a key is typed
    if (!(event.ctrlKey || event.metaKey || event.altKey)) {
      $currentInput.focus();
    }
    // When the client hits ENTER on their keyboard
    if (event.which === 13) {
      if (user) {
        sendMessage();
        socket.emit('stop typing');
        typing = false;
      } else {
        setUser();
      }
    }
  });

  $inputMessage.on('input', function() {
    updateTyping();
  });

  // Focus input when clicking on the message input's border
  $inputMessage.click(function () {
    $inputMessage.focus();
  });

  socket.on('add user', function (data) {
    var messages = data.messages;
    for (var i = 0; i < messages.length; i++) {
      addChatMessage(messages[i]);
    };
  });

  // Whenever the server emits 'login', log the login message
  socket.on('login', function (data) {
    connected = true;
    // Display the welcome message
    var message = "Interational Swagg Chat Room ";
    log(message, {
      prepend: true
    });
    addParticipantsMessage(data);

    var messages = data.messages;
    for (var i = 0; i < messages.length; i++) {
      addChatMessage(messages[i]);
    };
  });

  // Whenever the server emits 'new message', update the chat body
  socket.on('new message', function (data) {
    addChatMessage(data);
  });

  // Whenever the server emits 'user joined', log it in the chat body
  socket.on('user joined', function (data) {
    log(data.user.username + ' joined');
    addParticipantsMessage(data);
  });

  // Whenever the server emits 'user left', log it in the chat body
  socket.on('user left', function (data) {
    log(data.user.username + ' left');
    addParticipantsMessage(data);
    removeChatTyping(data);
  });

  // Whenever the server emits 'typing', show the typing message
  socket.on('typing', function (data) {
    addChatTyping(data.user.username);
  });

  // Whenever the server emits 'stop typing', kill the typing message
  socket.on('stop typing', function (data) {
    removeChatTyping(data.user.username);
  });
});
