jQuery(function($) {'use strict',

  DC_HOST = "http://localhost:5000";

  //Countdown js
 $("#countdown").countdown({
    date: "14 April 2017 18:00:00",
    format: "on"
  },

  function() {
    // callback function
  });

  function pageStatus(index) {
    if (index==0) { return "active" };
  }

  // Home page
  $("#event").find('.item').first().addClass("active");
  $("#instagram-feed").find('.item').first().addClass("active");
  $(".gallery").colorbox({rel:'gallery', maxHeight:'90%'})

  //Scroll Menu
  function menuToggle()
  {
    var windowWidth = $(window).width();

    if(windowWidth > 767 ){
      $(window).on('scroll', function(){
        if( $(window).scrollTop()>405 ){
          $('.main-nav').addClass('fixed-menu animated slideInDown');
        } else {
          $('.main-nav').removeClass('fixed-menu animated slideInDown');
        }
      });
    }else{

      $('.main-nav').addClass('fixed-menu animated slideInDown');

    }
  }


  menuToggle();


  // Carousel Auto Slide Off
  $('#event-carousel, #twitter-feed, #sponsor-carousel ').carousel({
    interval: false
  });


  // Contact form validation
  var form = $('.contact-form');
  form.submit(function () {'use strict',
    $this = $(this);
    $.post($(this).attr('action'), function(data) {
      $this.prev().text(data.message).fadeIn().delay(3000).fadeOut();
    },'json');
    return false;
  });

  $( window ).resize(function() {
    menuToggle();
  });

  $('.main-nav ul').onePageNav({
    currentClass: 'active',
      changeHash: false,
      scrollSpeed: 900,
      scrollOffset: 0,
      scrollThreshold: 0.3,
      filter: ':not(.no-scroll)'
  });
});

//Calling the locateme function when the document finishes loading
$(document).ready(function() {
  initMap();
});



// Google Map Customization
function initMap(){

  var map;

  map = new GMaps({
    el: '#gmap',
    lat: 18.00312126,
    lng: -76.82135493,
    scrollwheel:false,
    zoom: 17,
    zoomControl : false,
    panControl : false,
    streetViewControl : false,
    mapTypeControl: false,
    overviewMapControl: false,
    clickable: false
  });

  var image = "https://s3-us-west-2.amazonaws.com/damagecontrolfamily/assets/images/map-icon.png";
  map.addMarker({
    lat: 18.00312126,
    lng: -76.82135493,
    icon: image,
    animation: google.maps.Animation.DROP,
    verticalAlign: 'bottom',
    horizontalAlign: 'center',
    backgroundColor: '#3e8bff',
  });


  var styles = [

  {
    "featureType": "road",
    "stylers": [
    { "color": "#b4b4b4" }
    ]
  },{
    "featureType": "water",
    "stylers": [
    { "color": "#d8d8d8" }
    ]
  },{
    "featureType": "landscape",
    "stylers": [
    { "color": "#f1f1f1" }
    ]
  },{
    "elementType": "labels.text.fill",
    "stylers": [
    { "color": "#000000" }
    ]
  },{
    "featureType": "poi",
    "stylers": [
    { "color": "#d9d9d9" }
    ]
  },{
    "elementType": "labels.text",
    "stylers": [
    { "saturation": 1 },
    { "weight": 0.1 },
    { "color": "#000000" }
    ]
  }

  ];

  map.addStyle({
    styledMapName:"Styled Map",
    styles: styles,
    mapTypeId: "map_style"
  });

  map.setStyle("map_style");
};
