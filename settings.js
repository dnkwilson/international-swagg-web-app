module.exports = {
  POST_MAX_SIZE : 40 , //MB
  UPLOAD_MAX_FILE_SIZE: 40, //MB
  PROJECT_DIR : __dirname,
  S3_DIR: "https://" + process.env["AWS_BUCKET"] + ".s3.amazonaws.com"
};