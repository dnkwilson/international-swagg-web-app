var express = require('express');
var truncate = require('truncate');
var Party   = require('../models/Party');
var User    = require('../models/User');
var util = require('util');
var router = express.Router();

var request = require('request-json');
var api  = request.createClient(process.env["DC_API_HOST"]);

function setDefaultHeader(token){
  api.headers['Content-Type']  = 'application/json';
  api.headers['Accept']        = 'application/json';
  api.headers['Authorization'] = "Token token=\"" + token + "\"";
  api.headers['API-CALLER']    = "InternationalSwaggWebApp";
}


var isAuthenticated = function (req, res, next) {
  // if user is authenticated in the session, call the next() to call the next request handler
  // Passport adds this method to request object. A middleware is allowed to add properties to
  // request and response objects
  if (req.isAuthenticated())
    return next();
  // if the user is not authenticated then redirect him to the login page
  res.redirect('/login');
}

function groupParties(parties){
  var arrays = [], size = parties.length;
  while (parties.length > 0)
    arrays.push(parties.splice(0, 3));
  return arrays;
}

function getParties(array){
  var events = [];
  for (var i = 0; i < array.length; i++) {
    events.push(array[i]);
  };
  return groupParties(events)
}

function getPages(gallery){
  var pages = [];

  while (gallery.images.length > 0)
    pages.push(gallery.images.splice(0, 48));

  return pages;
}

function getImages(galleries) {
  var images = [];
  for (var i = 0; i < galleries.length; i++) {
    images.push(getPages(galleries[i]))
  }
  return images;
}

function getFeed(array){
  var feed = [];
  // for (var i = 0; i < array.length; i++) {
  //   array[i]['caption'] = truncate(array[i]['caption'], 157)
  //   feed.push(array[i]);
  // };
  return feed
}

module.exports = function(passport){
  /* GET home page. */
  router.get('/', function(req, res, next) {
    var token = "";

    if (req.user != null){ token = req.user.accessToken}
    setDefaultHeader(token);

    api.get('/internationalswagg', {}, function(err, response, body) {
      if (!err && res.statusCode == 200) {
        var events = getParties(body['events']);
        res.render('index', { page: "home",
                            path: res.path,
                            title: "International Swagg",
                            about: body['about'],
                            events: events,
                            feed: getFeed(body['ig_feed']),
                            user: req.user
                          });
      }
    });
  });

  // GET /chat
  router.get('/chat', isAuthenticated, function(req, res){
    res.render('chat', {page: "chat", path: res.path,
                        title: "International Swagg | Chat",
                        user: req.user});
  });

  router.get('/profile', isAuthenticated, function(req, res, next) {
    setDefaultHeader(req.user.accessToken);
    var hash = { provider: req.user.provider, uid: req.user.uid }
    api.post('/login', hash, function(err, response, body) {
      if (!err && response.statusCode == 200) {
        if (body.meta.is_logged_in == true){
          req.user.id       = body.user.id;
          req.user.username = body.user.username
          req.user.token    = body.user.token
          req.user.avatar   = body.user.profile_pic
          res.redirect('/');
        }else{
          res.redirect('/');
        }
      }else{
        req.logout();
        res.redirect('/')
      }
    });
  });

  // GET /login
  router.get('/login', function(req, res){
    if (req.isAuthenticated())
      res.redirect('/');
    res.render('login', {page: "login",
                        path: res.path,
                        title: "International Swagg | Login", });
  });

  // GET /terms
  router.get('/terms', function(req, res){
    res.render('terms', {page: "terms",
                        path: res.path,
                        title: "International Swagg | Terms & Conditions", });
  });

  // GET /privacy
  router.get('/privacy', function(req, res){
    res.render('privacy', {page: "privacy",
                        path: res.path,
                        title: "International Swagg | Privacy Policy", });
  });

  // GET /logout
  router.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
  });

  // route for facebook authentication and login
  // different scopes while logging in
  router.get('/login/facebook',
    passport.authenticate('facebook', { scope : 'email' }),
    function(req, res){
      // The request will be redirected to Facebook for authentication, so this
      // function will not be called.
    }
  );

  // handle the callback after facebook has authenticated the user
  router.get('/login/facebook/callback',
    passport.authenticate('facebook', {
      successRedirect : '/profile',
      failureRedirect : '/'
    })
  );

  // route for google authentication and login
  // different scopes while loggingoogleg in
  router.get('/login/google',
    passport.authenticate('google', { scope : 'email' }),
    function(req, res){
      // The request will be redirected to Facebook for authentication, so this
      // function will not be called.
    }
  );

  // handle the callback after facebook has authenticated the user
  router.get('/login/google/callback',
    passport.authenticate('google', {
      successRedirect : '/profile',
      failureRedirect : '/'
    })
  );

  router.get('/events/:id', function(req, res, next) {
    var token = "";

    if (req.user != null){ token = req.user.accessToken}
    setDefaultHeader(token);
    api.get('/events/' + req.params.id, {}, function(err, response, body) {
      if (!err && response.statusCode == 200) {
        res.render('event', {party: body.event,
                              galleries: getImages(body.event.galleries),
                              page: "event",
                              title: "International Swagg",
                              user: req.user});
      }
    });
  });

  return router;
}
