var fs = require('fs');
var s3fs = require('../config/s3fs');
var path = require('path');
var settings = require('../settings')
var util = require('util');

var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

var urlencodedParser = bodyParser.urlencoded({ extended: false })

var redis = require('redis');
var db = redis.createClient(process.env['REDIS_URL']);

module.exports = function(app) {
  function getCategory(category){
    if (category == "photo_message") { return "photos" } else{ return "voicenotes" };
  }

  function getTokens() {
    return db.lrange("chat:excluded_tokens", 0, -1);
  }

  app.get('/history', function (req, reply) {
    db.lrange("chat:messages", -50, -1, function (err, data) {
      reply.send({history: data});
    });
  });

  app.get('/excluded_tokens', function(req, reply){
    db.lrange("chat:excluded_tokens", 0, -1, function(err, data){
      reply.send({excluded_tokens: getTokens()});
    });
  });

  app.post('/upload', jsonParser, function(req, res) {
    if (!req.body) return res.sendStatus(400);

    var body = req.body;
    var mediaFile = body['media']['media_file'];
    var filename = body['media']['filename'];


    var file = new Buffer(mediaFile, 'base64');
    var uploadPath;

    if (process.env["NODE_ENV"] == "development"){
      uploadPath = "/uploads/" + filename;
      var uploadDir =  path.join(settings.PROJECT_DIR + "/public/uploads");
      var filePath = uploadDir + "/" + filename;
      fs.writeFile(filePath, file, function (err) {
        if(err) res.send({err: err});
        res.send({success: true, media: "http://10.0.3.2:4000" + uploadPath});
      });
    }else{
      uploadPath = "/chat/" + getCategory(body["category"]) + "/" + filename;
      s3fs.writeFile(uploadPath, file, function (err) {
        if (err) throw err;
        res.send({success: true, media: settings.S3_DIR + uploadPath});
      });
    }
  });


  app.get('/uploads/:file', function (req, res){
    file = req.params.file;
    var dirname = "../public/uploads/";
    var img = fs.readFileSync(dirname + "/uploads/" + file);
    res.writeHead(200, {'Content-Type': 'image/jpg' });
    res.end(img, 'binary');
  });

  app.get('/download', function(req, res) {
    res.redirect('https://play.google.com/store/apps/details?id=com.dnwilson.internationalswagg');
  })
};
