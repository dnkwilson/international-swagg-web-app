var method = Party.prototype;
var truncate = require('truncate');

function Party(opts) {
  this._id           = opts['id'];
  this._name         = truncate(opts['name'], 25);
  this._venueId      = opts['venue_id'];
  this._description  = opts['description'];
  this._date         = opts['date'];
  this._flyerSm      = opts['flyer']['sm']['url'];
  this._flyerThumb   = opts['flyer']['thumb']['url'];
  this._flyerLg      = opts['flyer']['lg']['url'];
}

method.getId = function() {
  return this._id;
};

method.getName = function() {
  return this._name;
};

method.getVenueId = function() {
  return this._venueId;
};

method.getDescription = function() {
  return this._description;
};

method.getDate = function() {
  return this._date;
};

method.getFlyerSm = function() {
  return this._flyerSm;
};

method.getFlyerThumb = function() {
  return this._flyerThumb;
};

method.getFlyerLg = function() {
  return this._flyerLg;
};

method.toJSON = function(){
  return {
            id: this._id,
            name: this._name,
            venue_id: this._venueId,
            description: this._description,
            date: this._date,
            flyer_sm: this._flyerSm,
            flyer_thumb: this._flyerThumb,
            flyer_lg: this._flyerLg
          }
}

Party.groupEvents = function(parties){
  var arrays = [], size = parties.length;
  while (parties.length > 0)
    arrays.push(parties.splice(0, 3));
  return arrays;
}

Party.getParties= function(array){
  var events = [];
  for (var i = 0; i < array.length; i++) {
    var party = new Party(array[i]);
    events.push(party);
  };
  return Party.groupEvents(events)
}

module.exports = Party;
