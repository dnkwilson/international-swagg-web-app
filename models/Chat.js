var method = Chat.prototype;

var redis = require('redis');
var db = redis.createClient(process.env['REDIS_URL']);
var moment = require('moment');

const TEXT_MESSAGE   = "text_message";
const VOICE_MESSAGE  = "voice_message";
const PHOTO_MESSAGE  = "photo_message";
const CHAT_DB        = "chat:messages";

function Chat(opts) {
  this._userId = opts['user_id'];
  this._username = opts['username'];
  this._user_color = opts['userColor']
  this._avatar = opts['avatar'];
  this._body = opts['body'];
  this._time = moment().format("YYYY-MM-DD HH:mm:ss Z");
  this._category = opts['category'];
  if (this._category != TEXT_MESSAGE) {
    this._filename = opts['media']['filename'];
    this._media = opts['media']['media_file'];
    if (this._category == VOICE_MESSAGE) {
      this._duration = opts['media']['duration'];
    };
  };
}

method.getUserId = function() {
  return this._userId;
};

method.getUserName = function() {
  return this._username;
};

method.getBody = function() {
  return this._body;
};

method.getTime = function() {
  return this._time;
};

method.getCategory = function() {
  return this._category;
};

method.getFileName = function() {
  return this._filename;
};

method.getMedia = function() {
  return this._media;
};

method.getDuration = function() {
  return this._duration;
};

method.toJSON = function(){
  return {
            user_id: this._userId,
            username: this._username,
            avatar: this._avatar,
            category: this._category,
            time: this._time,
            body: this._body,
            media: this._media,
            filename: this._filename,
            duration: this._duration
          }
}

method.save = function() {
  var str = JSON.stringify(this);
  db.rpush("chat:messages", str);
};

module.exports = Chat;
